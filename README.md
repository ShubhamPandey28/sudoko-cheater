# Sudoku-cheater

A GUI app that solve and saves realtime sudoku from the video input.

### Running instructions

clone the repo ``` git clone https://gitlab.com/ShubhamPandey28/sudoko-cheater.git ``` <br>
run ```python3 gui.py``` <br>
place the sudoku in front of camera <br>
save the solution using save button.

