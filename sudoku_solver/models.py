import tensorflow as tf
import tensorflow_datasets as tfds

from tensorflow.keras import layers, Model, Sequential

INPUT_SHAPE = [28, 28, 1]

def make_model():
    inp = layers.Input(shape=INPUT_SHAPE)

    layer = Sequential()
    layer.add(layers.Conv2D(32, kernel_size=(5,5),strides=(2,2), padding='same', use_bias=False))
    layer.add(layers.BatchNormalization())
    layer.add(layers.LeakyReLU())
    layer.add(layers.ZeroPadding2D())
    layer.add(layers.Conv2D(32, kernel_size=(5,5),strides=(2,2), padding='same', use_bias=False))
    layer.add(layers.BatchNormalization())
    layer.add(layers.LeakyReLU())
    layer.add(layers.ZeroPadding2D())
    layer.add(layers.Conv2D(32, kernel_size=(5,5), strides=(2,2), padding='same', use_bias=False))
    layer.add(layers.BatchNormalization())
    layer.add(layers.LeakyReLU())
    layer.add(layers.Flatten())
    layer.add(layers.Dense(10, activation='sigmoid'))

    x = inp
    x = layer(x)

    return Model(inputs = inp, outputs = x)
