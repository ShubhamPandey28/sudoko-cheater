import os
import cv2
import numpy as np
import imutils
import tensorflow as tf

from matplotlib import pyplot as plt
from tensorflow.keras.models import load_model
from imutils.perspective import four_point_transform
from skimage.segmentation import clear_border

from .models import make_model

predictor = load_model(os.path.join(os.path.dirname(__file__),'tf-model.h5'), compile=False)

def locate_puzzle(image):
    
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (7, 7), 3)
    thresh = cv2.adaptiveThreshold(blurred, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
    thresh = cv2.bitwise_not(thresh)

    contours = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = imutils.grab_contours(contours)
    contours = sorted(contours, key=cv2.contourArea, reverse=True)

    main_poly = None

    for contour in contours:
        epsilon = 0.02*cv2.arcLength(contour, closed=True)
        poly = cv2.approxPolyDP(contour, epsilon,closed=True)

        if len(poly) == 4:
            main_poly = poly
            break
    
    # print(main_poly)

    puzzle = four_point_transform(image, main_poly.reshape(4, 2))
    wrap = four_point_transform(gray, main_poly.reshape(4, 2))

    cv2.imwrite('img-t.jpg', wrap)

    return puzzle, wrap


def get_digit(cell):

    thresh = cv2.threshold(cell, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
    thresh = clear_border(thresh)

    contours = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = imutils.grab_contours(contours)

    if len(contours) == 0:
        return []
    
    main_contour = max(contours, key=cv2.contourArea)
    mask = np.zeros(thresh.shape, dtype='uint8')
    cv2.drawContours(mask, [main_contour],-1, 255, -1)

    (h, w) = thresh.shape

    if cv2.countNonZero(mask)/float(w*h) < 0.03:
        return []
    
    digit = cv2.bitwise_and(thresh, thresh, mask=mask)

    return digit


def get_board(image):

    puzzle, wrap = locate_puzzle(image)
    board = np.zeros((9, 9), dtype='int')
    dx, dy = wrap.shape[1]//9, wrap.shape[0]//9
    

    cells = []

    for y in range(9):
        row = []
        for x in range(9):
            lower_x, lower_y = x*dx, y*dy
            upper_x, upper_y = (x+1)*dx, (y+1)*dx
            row.append((lower_x, lower_y, upper_x, upper_y))

            digit = get_digit(wrap[lower_y:upper_y, lower_x:upper_x])

            if len(digit):
                img = cv2.resize(digit, (28,28))
                img = img.astype('float') / 127.5 - 1
                img = tf.stack([img], axis=0)
                pred = predictor(img)[0].numpy().argmax()
                '''
                plt.imshow((img[0]+1)*127.5)
                plt.title(str(pred))
                plt.show()
                '''
                board[y, x] = pred
        
        cells.append(row)
    
    return board
