import os
import sys
import time
import cv2
import datetime
import tkinter
import PIL.Image, PIL.ImageTk

from tkinter import messagebox
from sudoku_solver.solver import solve_sudoku
from sudoku_solver.cv_utils import get_board

 
class App:
    def __init__(self, window, window_title, video_source=0):
        self.window = window
        self.window.title(window_title)
        self.video_source = video_source

        self.vid = MyVideoCapture(self.video_source)


        self.canvas = tkinter.Canvas(window, width = self.vid.width, height = self.vid.height)
        self.canvas.pack()

        self.btn_solve=tkinter.Button(window, text="Solve", width=50, command=self.solve)
        self.btn_solve.pack(anchor=tkinter.CENTER, expand=True)
        self.btn_save = tkinter.Button(window,  text='Save', width=50, command=self.save)
        self.btn_save.pack(anchor=tkinter.CENTER, expand=True)

        self.delay = 15
        self.update()

        self.window.mainloop()

    def solve(self):
        ret, frame = self.vid.get_frame()

        board = None
        try:
            board = get_board(frame)
        except:
            print('Unable to get the board')

        if not board is None:
            try:
                soln = solve_sudoku((3,3), board)

            except:
                messagebox.showerror("Solving Error", "Please try facing the sudoku correctly")
                soln =  None

    def save(self):
        ret, frame = self.vid.get_frame()

        board = None
        try:
            board = get_board(frame)
        except:
            print('Unable to get the board')

        if not board is None:
            try:
                soln = solve_sudoku((3,3), board)
                with open('solution.txt', 'w') as f:
                    s = ''
                    for i in soln:
                        s += '\n' + '-'.join(i)
                    f.write(s)

            except:
                messagebox.showerror("Solving Error", "Please try facing the sudoku correctly")
                soln =  None
        
        

    def update(self):
        ret, frame = self.vid.get_frame()        

        if ret:
            self.photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(frame))
            self.canvas.create_image(0, 0, image = self.photo, anchor = tkinter.NW)

        self.window.after(self.delay, self.update)


class MyVideoCapture:
    def __init__(self, video_source=0):
        self.vid = cv2.VideoCapture(video_source)
        if not self.vid.isOpened():
            raise ValueError("Unable to open video source", video_source)

        self.width = self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)

    def get_frame(self):
        if self.vid.isOpened():
            ret, frame = self.vid.read()
            if ret:
                return (ret, cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
            else:
                return (ret, None)
        else:
            return (ret, None)

    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()

if __name__ == '__main__':
    App(tkinter.Tk(), 'SUDOKU CHEATER')
